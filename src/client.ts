import axios from 'axios'
import * as path from 'path'

const socketPath = process.platform === 'win32' ? '\\\\.\\pipe\\aorta-test' : path.resolve('./socket/aorta-test.sock')

  ; (async function() {
    const { data } = await axios.get('/', { socketPath })
    console.log(data)
  })()

