import Koa from 'koa'
import KoaBodyParser from 'koa-bodyparser'
import KoaRouter from 'koa-router'
import * as path from 'path'

const socketPath = process.platform === 'win32' ? '\\\\.\\pipe\\aorta-test' : path.resolve('./socket/aorta-test.sock')

const server = new Koa()

const router = new KoaRouter()

router.get('/', async ctx => {
  ctx.body = { message: 'hello' }
})

const bodyParser = KoaBodyParser()

server.use(bodyParser)
server.use(router.routes())

server.listen(socketPath, () => {
  console.log(`Server listening on ${socketPath}`)
})
