declare module 'node-windows' {
  class EventLogger {
    constructor(name: string)
    info(message: string): void
    warn(message: string): void
    error(message: string): void
  }

  interface ServiceInitOptions {
    name: string
    description: string
    script: string
    nodeOptions?: string[]
    workingDirectory?: string
    allowServiceLogon?: boolean
  }

  type ServiceEvent = 'install' | 'alreadyinstalled' | 'invalidinstallation' | 'uninstall' | 'alreadyuninstalled' | 'start' | 'stop' | 'error'

  class Service {
    constructor(options: ServiceInitOptions)

    on(event: ServiceEvent, callback: Function): void

    install(): void

    uninstall(): void
  }
}
