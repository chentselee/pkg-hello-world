import * as fs from 'fs'
import * as path from 'path'
import { EventLogger, Service } from 'node-windows'

console.log("hello world from mac")

const filePath = path.resolve('./umask.txt')
if (fs.existsSync(filePath)) {
  fs.unlinkSync(filePath)
}
process.umask(0o022)
fs.writeFileSync(filePath, "0o022 maybe")

if (process.platform === 'win32') {
  const logger = new EventLogger('helloworld')
  logger.info('this is a info')
  logger.warn('this is a warning')

  const service = new Service({ name: 'helloworld-server', description: 'test server', script: path.resolve(__dirname, 'dist', 'server.js') })

  service.on('install', () => {
    logger.info('start background service')
  })
  service.on('uninstall', () => {
    logger.info('background service stopped')
  })

  process.on('SIGINT', () => {
    logger.info('receive SIGINT')
    service.uninstall()
  })
  process.on('SIGTERM', () => {
    logger.info('receive SIGINT')
    service.uninstall()
  })

  service.install()
}
